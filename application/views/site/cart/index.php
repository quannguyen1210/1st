<style>table#cart_contents td{padding: 10px;border: 1px solid #ccc}</style>
<div class="box-center"><!-- The box-center product-->
             <div class="tittle-box-center">
		        <h2>Thông Tin Giỏ Hàng  có <?php echo $total_items ?> Sản Phẩm</h2>
		      </div>
		      <div class="box-content-center product"><!-- The box-content-center -->
		     <?php if($total_items > 0) :?>
		      <form action="<?php echo base_url('cart/update')?>" method ="post">
		      	<table id ="cart_contents">
		      		<thead>
		      			<th>Sản Phẩm</th>
		      			<th>Giá</th>
		      			<th>Số Lượng</th>
		      			<th>Tổng Số</th>
		      			<th>Xóa</th>
		      		</thead>
		      		<tbody>
		      			<?php $total_amount = 0; ?>
		      			<?php foreach ($carts as $key => $row) :?>
		      			<?php $total_amount +=  $row['subtotal']; ?>
		      			<tr>
		      				<td>
		      					<?php echo $row['name']; ?>
		      				</td>
		      				<td>
		      					<?php echo number_format($row['price']); ?>
		      				</td>
		      				<td>
		      					<input name="qty_<?php echo $row['id'] ?>" value="<?php echo $row['qty']; ?>" size="5" >
		      				</td>
		      				<td>
		      					<?php echo number_format($row['subtotal']); ?> đ
		      				</td>
		      				<td><a href="<?php echo base_url('cart/delete/'.$row['id'])?>">Xóa</a></td>
		      			</tr>
		      			<?php endforeach ;?>

		      			<tr>
		      				<td colspan="5">Tổng Số Tiền: <b style="color: red"><?php echo number_format($total_amount) ?>đ</b> -
		      				 <a href="<?php echo base_url('cart/delete') ?>">Xóa toàn bộ</a>
		      				 </td>
		      			</tr>
		      			<tr>
		      				<td colspan="5"><button type="submit">Cập Nhật</button></td>
		      			</tr>

		      		</tbody>
		      	</table>
		      </form>
		  <?php else:?>
		  	<h4>Không có sản phẩm trong giỏ hàng</h4>
		  <?php endif;?>
		      </div>
</div>