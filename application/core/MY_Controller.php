<?php
	Class MY_Controller extends CI_Controller
	{
		//bien gui du lieu sang ben view
		public $data = array();
		function __construct()
		{
			//ke thừa CI_Controll
			parent::__construct();

			$controller = $this->uri->segment(1);
			switch ($controller) {
					case 'admin':
					{	
						$this->load->helper('admin');
						$this->_check_login();
						break;
					}
					default:
					{
						//xu ly du lieu trang ngoai
						//lay danh muc san pham
						$this->load->model('catalog_model');
						$input = array();
						$input['where'] = array('parent_id' => 0);
						$catalog_list = $this->catalog_model->get_list($input);
						foreach ($catalog_list as $row)
						{
							$input['where'] = array('parent_id' => $row->id);
							$subs = $this->catalog_model->get_list($input);
							$row->subs = $subs;

						}
						$this->data['catalog_list'] = $catalog_list;

						//lay danh sach bai viet moi
						$this->load->model('news_model');
						$input = array();
						$input['limit'] = array(5,0);
						$news_list = $this->news_model->get_list($input);
						$this->data['news_list'] = $news_list;

						//load thu vien
						$this->load->library('cart');
						$this->data['total_items'] = $this->cart->total_items();
					}
				}	
		}
		private function _check_login()
		{
			$controller = $this->uri->rsegment('1');
			$controller = strtolower($controller);

			$login = $this->session->userdata('login');
			//neu ma chua dang nhap,mà truy cap vao controller khac login
			if (!$login && $controller != 'login')
			{
				redirect(admin_url('login'));
			}
			//neu admin dang nhap,thì ko vào login 
			if($login && $controller == 'login')
			{
				redirect(admin_url('home'));
			}
		}
	}
?>