<?php
	Class Admin extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('admin_model');
		}

		// lấy danh sách admin
		function index()
		{
			$input = array();
			$list = $this->admin_model->get_list($input);
			$this->data['list'] = $list;
			
			$total = $this->admin_model->get_total();
			$this->data['total'] = $total;

			//lấy nội dung của biến message
			$message = $this->session->flashdata('message');
			$this->data['message'] = $message;
			$this->data['temp']='admin/admin/index';
			$this->load->view('admin/main',$this->data);
		}
		// kiểm tra username
		function check_username()
		{
			$username = $this->input->post('username');
			$where = array('username' => $username);
			//kiểm tra username tồn tại
			if ($this->admin_model->check_exists($where))
			{
				// Thông báo lỗi
				$this->form_validation->set_message(__FUNCTION__,'Tài Khoản Tồn Tại');
				return false;
			}
			return true;
		}
		// thêm mới quản trị viên
		function add()
		{
			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','Tên','required|min_length[3]');
				$this->form_validation->set_rules('username','Username','required|callback_check_username');
				$this->form_validation->set_rules('password','Password','required|min_length[6]');
				$this->form_validation->set_rules('re_password','Password','required|min_length[6]|matches[password]');
				$this->form_validation->set_rules('decrip','Mô Tả');
			//nếu nhập chính xác
				if ($this->form_validation->run())
				{
					//them giu liệu
					$name = $this->input->post('name');
					$username = $this->input->post('username');
					$password = $this->input->post('password');
					$data = array(
						'name' => $name,
						'username' =>$username,
						'password' =>md5($password)
					);
					if($this->admin_model->create($data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Thêm Thành Công');
					}else{
						$this->session->set_flashdata('message','Không Thành Công');
					}
					// chuyển tới trang danh sách quản trị viên
					redirect(admin_url('admin'));
				}
			}
			$this->data['temp']='admin/admin/add';
			$this->load->view('admin/main',$this->data);
		}
		//Chỉnh sửa thông tin quản trị viên
		function edit()
		{
			$id = $this->uri->rsegment('3');
			$id = intval($id);
			//lay thong tin quan tri vien
			$info = $this->admin_model->get_info($id);
			if (!$info)
			{
				$this->session->set_flashdata('message','Không Tồn Tại Quản Trị Viên');
				redirect(admin_url('admin'));
			}
			$this->data['info']= $info;

			if ($this->input->post())
			{
				$this->form_validation->set_rules('name','Tên','required|min_length[3]');
				$this->form_validation->set_rules('username','Username','required|callback_check_username');

				$password =$this->input->post('password');
				if($password)
				{
				$this->form_validation->set_rules('password','Password','required|min_length[6]');
				$this->form_validation->set_rules('re_password','Password','required|min_length[6]|matches[password]');
				}
				$this->form_validation->set_rules('decrip','Mô Tả');
				if ($this->form_validation->run())
				{
				//them giu liệu
					$name = $this->input->post('name');
					$username = $this->input->post('username');
					
					$data = array(
						'name' => $name,
						'username' =>$username,
					);
					//nếu thay đổi mật khẩu thì gán dữ liệu
					if($password)
					{
						$data['password'] = md5($password);
					}
					if($this->admin_model->update($id,$data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Cập Nhật Thành Công');
					}else{
						$this->session->set_flashdata('message','Không Cập Nhật Thành Công');
					}
					// chuyển tới trang danh sách quản trị viên
					redirect(admin_url('admin'));
				}
			}
			$this->data['temp']='admin/admin/edit';
			$this->load->view('admin/main',$this->data);
		}
		//Hàm xóa dữ liệu
		function delete()
		{
			$id = $this->uri->rsegment('3');
			$id = intval($id);
			//lấy thông tin quản trị viên
			$info = $this->admin_model->get_info($id);
			if (!$info)
			{
				$this->session->set_flashdata('message','Không tồn tại quản trị viên');
				redirect(admin_url('admin'));
			}
			// xóa 
			$this->admin_model->delete($id);

			$this->session->set_flashdata('message','Xóa dữ liệu thành công');
			redirect(admin_url('admin'));
		}
		//đăng xuất
		function logout()
		{
			if ($this->session->userdata('login'))
			{
				$this->session->unset_userdata('login');
			}
			redirect(admin_url('login'));
		}
	}
?>