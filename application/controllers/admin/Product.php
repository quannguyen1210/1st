<?php
	Class Product extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('product_model');
		}

		//hien thi danh sach san pham
		function index()
		{
			//lay tong so luong tat ca san pham
			$total_rows = $this->product_model->get_total();
			$this->data['total_rows'] = $total_rows;

			$config = array();
			$config['total_rows'] = $total_rows;// tong tat ca cac san pham
			$config['base_url'] = admin_url('product/index/');//link hien thi danh sach san pham
			$config['per_page'] = 5;//so luong san pham hien thi
			$config['uri_segment'] = 4;// phan doan hien thi so trang url
			$config['next_link'] = 'Trang Kế';
			$config['prev_link'] = 'Trang Trước';
			//khởi tạo cấu hình pagination
			$this->pagination->initialize($config); 

			$segment = $this->uri->segment(4);
			$segment = intval($segment);

			$input = array();
			$input['limit'] = array($config['per_page'],$segment);

			//lay danh sach san pham
			$list = $this->product_model->get_list($input);
			$this->data['list'] = $list;

			// kiem tra co thuc hien loc khong
			$id = $this->input->get('id');
			$id = intval($id);
			$input['where']= array();
			if($id > 0)
			{
				$input['where']['id']= $id;
			}
			$name = $this->input->get('name');
			if ($name)
			{
				$input['like'] = array('name' ,$name);
			}
			$catalog_id = $this->input->get('catalog');
			$catalog_id = intval($catalog_id);
			if ($catalog_id>0)
			{
				$input['where']['catalog_id']= $catalog_id;
			}
			
			//lay danh sach danh muc san pham
			$this->load->model('catalog_model');
			$input = array();
			$input['where'] = array('parent_id' => 0);
			$catalog = $this->catalog_model->get_list($input);
			foreach ($catalog as $row)
			{
				$input['where'] = array('parent_id' => $row->id);
				$subs = $this->catalog_model->get_list($input);
				$row->subs = $subs;
			}
			$this->data['catalog'] = $catalog;

			//lay doi dung bien message
			$message = $this->session->flashdata('message');
			$this->data['message'] = $message;

			//load view
			$this->data['temp'] = 'admin/product/index';
			$this->load->view('admin/main',$this->data);
		}
		function add()
		{
						//lay danh sach danh muc san pham
			$this->load->model('catalog_model');
			$input = array();
			$input['where'] = array('parent_id' => 0);
			$catalogs = $this->catalog_model->get_list($input);
			foreach ($catalogs as $row)
			{
				$input['where'] = array('parent_id' => $row->id);
				$subs = $this->catalog_model->get_list($input);
				$row->subs = $subs;
			}
			$this->data['catalog'] = $catalogs;


			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','Tên','required');
				$this->form_validation->set_rules('catalog','Thể Loại','required');
				$this->form_validation->set_rules('price','Giá','required');

			//nếu nhập chính xác
				if ($this->form_validation->run())
				{
					//them giu liệu
					$name = $this->input->post('name');
					$catalog_id = $this->input->post('catalog');
					$price = $this->input->post('price');
					$price = str_replace(',', '', $price);

					//lay ten file anh duoc update len
					$this->load->library('upload_library');
					$upload_path ='./upload/product'; 
					$upload_data = $this->upload_library->upload($upload_path,'image');
					$image_link = '';
					if (isset($upload_data['file_name']))
					{
						$image_link = $upload_data['file_name'];
					}
					//upload cac hinh kem theo
					$image_list = array();
					$image_list = $this->upload_library->upload_file($upload_path,'image_list[]');
					$image_list = json_encode($image_list);
					//du lieu can them
					$data = array(
						'name' 		 => $name,
						'catalog_id' => $catalog_id,
						'price' 	 => $price,
						'image_link' => $image_link,
						'image_list' => $image_list,
						
						'discount'	 => $this->input->post('discount'),
						'warranty'	 => $this->input->post('warranty'),
						'gifts'		 => $this->input->post('gifts'),
						'content'	 => $this->input->post('content')
					);
					//them moi vao csdl
					if($this->product_model->create($data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Thêm Thành Công');
					}else{
						$this->session->set_flashdata('message','Không Thành Công');
					}
					// chuyển tới trang danh sách 
					redirect(admin_url('product'));
				}
			}


			$this->data['temp'] = 'admin/product/add';
			$this->load->view('admin/main',$this->data);
			
		}
		function edit()
		{
			$id = $this->uri->rsegment('3');
			$product = $this->product_model->get_info($id);
			if (!$product)
			{
				//tạo thông báo
				$this->session->set_flashdata('message','Không tồn tại ID');
				redirect(admin_url('product'));
			}
			$this->data['product'] = $product;
			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','Tên','required');
				$this->form_validation->set_rules('catalog','Thể Loại','required');
				$this->form_validation->set_rules('price','Giá','required');



			//nếu nhập chính xác
				if ($this->form_validation->run())
				{
					//them giu liệu
					$name = $this->input->post('name');
					$catalog_id = $this->input->post('catalog');
					$price = $this->input->post('price');
					$price = str_replace(',', '', $price);

					//lay ten file anh duoc update len
					$this->load->library('upload_library');
					$upload_path ='./upload/product'; 
					$upload_data = $this->upload_library->upload($upload_path,'image');
					
					$image_link = '';
					if (isset($upload_data['file_name']))
					{
						$image_link = $upload_data['file_name'];
					}
					//upload cac hinh kem theo
					$image_list = array();
					$image_list = $this->upload_library->upload_file($upload_path,'image_list[]');
					$image_list_json = json_encode($image_list);
					//du lieu can them
					$data = array(
						'name' 		 => $name,
						'catalog_id' =>$catalog_id,
						'price' 	 =>$price,

						'discount' 	 =>$this->input->post('discount'),
						'warranty'	 =>$this->input->post('warranty'),
						'gifts'	     =>$this->input->post('gifts'),
						'content'	 =>$this->input->post('content')
					);
					if ($image_link != "")
					{
						$data['image_link'] = $image_link;
					}
					if (!empty($image_list))
					{
						$data['image_list'] = $image_list_json;
					}

					//cap nhat vao csdl
					if($this->product_model->update($product->id,$data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Cập Nhật Thành Công');
					}else{
						$this->session->set_flashdata('message','Cập Nhật Không Thành Công');
					}
					// chuyển tới trang danh sách 
					redirect(admin_url('product'));
				}
			}
			//lay danh sach danh muc san pham
			$this->load->model('catalog_model');
			$input = array();
			$input['where'] = array('parent_id' => 0);
			$catalog = $this->catalog_model->get_list($input);
			foreach ($catalog as $row)
			{
				$input['where'] = array('parent_id' => $row->id);
				$subs = $this->catalog_model->get_list($input);
				$row->subs = $subs;
			}
			$this->data['catalog'] = $catalog;


			$this->data['temp'] = 'admin/product/edit';
			$this->load->view('admin/main',$this->data);
		}
		//Hàm xóa dữ liệu
		function delete()
		{
			$id = $this->uri->rsegment('3');
			$id = intval($id);
			//lấy thông tin sản phẩm
			$product = $this->product_model->get_info($id);
			if (!$product)
			{
				$this->session->set_flashdata('message','Không tồn tại sản phẩm');
				redirect(admin_url('product'));
			}
			// xóa 
			$this->product_model->delete($id);
			//tao noi dung thong bao
			$this->session->set_flashdata('message','Xóa sản phẩm thành công');
			redirect(admin_url('product'));
			//xoa anh dai dien
			$image_link = './upload/product'.$product->images_link;
			if (file_exists($image_link))
			{
				unlink($image_link);
			}
			//xóa anh kem theo
			$image_list = json_decode($product->image_list);
			if (is_array($image_list))
			{
				foreach ($image_list as $img)
				{
					$image_link = './upload/product'.$product->$img;
					if (file_exists($image_link))
					{
						unlink($image_link);
					}
				}
			}
			$this->session->set_flashdata('message','Xóa sản phẩm thành công');
			redirect(admin_url('product'));
		}
	}
?>