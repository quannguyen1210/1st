<?php
	Class News extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('news_model');
		}

		//hien thi danh sach san pham
		function index()
		{
			//lay tong so luong tat ca san pham
			$total_rows = $this->news_model->get_total();
			$this->data['total_rows'] = $total_rows;

			$config = array();
			$config['total_rows'] = $total_rows;// tong tat ca cac san pham
			$config['base_url'] = admin_url('news/index/');//link hien thi danh sach san pham
			$config['per_page'] = 5;//so luong san pham hien thi
			$config['uri_segment'] = 4;// phan doan hien thi so trang url
			$config['next_link'] = 'Trang Kế';
			$config['prev_link'] = 'Trang Trước';
			//khởi tạo cấu hình pagination
			$this->pagination->initialize($config); 

			$segment = $this->uri->segment(4);
			$segment = intval($segment);

			$input = array();
			$input['limit'] = array($config['per_page'],$segment);
			// kiem tra co thuc hien loc khong
			$id = $this->input->get('id');
			$id = intval($id);
			$input['where']= array();
			if($id > 0)
			{
				$input['where']['id']= $id;
			}
			$title = $this->input->get('title');
			if ($title)
			{
				$input['like'] = array('title' ,$title);
			}
			
			//lay danh sach bai viet
			$list = $this->news_model->get_list($input);
			$this->data['list'] = $list;
			
			//lay doi dung bien message
			$message = $this->session->flashdata('message');
			$this->data['message'] = $message;

			//load view
			$this->data['temp'] = 'admin/news/index';
			$this->load->view('admin/main',$this->data);
		}
		//them bai viet moi
		function add()
		{
			

			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('title','Tiêu đề bài viết','required');
				$this->form_validation->set_rules('content','Nội dung bài viết','required');
		
			//nếu nhập chính xác
				if ($this->form_validation->run())
				{

					//lay ten file anh duoc update len
					$this->load->library('upload_library');
					$upload_path ='./upload/news'; 
					$upload_data = $this->upload_library->upload($upload_path,'image');
					$image_link = '';
					if (isset($upload_data['file_name']))
					{
						$image_link = $upload_data['file_name'];
					}
					
					//du lieu can them
					$data = array(
						'title' 	 => $this->input->post('title'),
						'content' 	 => $this->input->post('content'),
						'image_link' => $image_link,

					);
					//them moi vao csdl
					if($this->news_model->create($data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Thêm Thành Công');
					}else{
						$this->session->set_flashdata('message','Không Thành Công');
					}
					// chuyển tới trang danh sách 
					redirect(admin_url('news'));
				}
			}


			$this->data['temp'] = 'admin/news/add';
			$this->load->view('admin/main',$this->data);
			
		}
		function edit()
		{
			$id = $this->uri->rsegment('3');
			$news = $this->news_model->get_info($id);
			if (!$news)
			{
				//tạo thông báo
				$this->session->set_flashdata('message','Không tồn tại ID');
				redirect(admin_url('news'));
			}
			$this->data['news'] = $news;
			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('title','Tiêu đề bài viết','required');
				$this->form_validation->set_rules('content','Nội dung bài viết','required');



			//nếu nhập chính xác
				if ($this->form_validation->run())
				{


					//lay ten file anh duoc update len
					$this->load->library('upload_library');
					$upload_path ='./upload/news'; 
					$upload_data = $this->upload_library->upload($upload_path,'image');
					
					$image_link = '';
					if (isset($upload_data['file_name']))
					{
						$image_link = $upload_data['file_name'];
					}

					//du lieu can them
					$data = array(
						'title' 		 => $title,
						'content'	 =>$this->input->post('content')
					);
					if ($image_link != "")
					{
						$data['image_link'] = $image_link;
					}


					//cap nhat vao csdl
					if($this->news_model->update($news->id,$data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Cập Nhật Thành Công');
					}else{
						$this->session->set_flashdata('message','Cập Nhật Không Thành Công');
					}
					// chuyển tới trang danh sách 
					redirect(admin_url('news'));
				}
			}
			//lay danh sach danh muc san pham


			$this->data['temp'] = 'admin/news/edit';
			$this->load->view('admin/main',$this->data);
		}
		//Hàm xóa dữ liệu
		function delete()
		{
			$id = $this->uri->rsegment('3');
			$id = intval($id);
			//lấy thông tin sản phẩm
			$news = $this->news_model->get_info($id);
			if (!$news)
			{
				$this->session->set_flashdata('message','Không tồn tại sản phẩm');
				redirect(admin_url('news'));
			}
			// xóa 
			$this->news_model->delete($id);
			//tao noi dung thong bao
			$this->session->set_flashdata('message','Xóa thành công');
			redirect(admin_url('news'));
			//xoa anh dai dien
			$image_link = './upload/news'.$news->images_link;
			if (file_exists($image_link))
			{
				unlink($image_link);
			}
			//xóa anh kem theo
			
			$this->session->set_flashdata('message','Xóa thành công');
			redirect(admin_url('news'));
		}
	}
?>