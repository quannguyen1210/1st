<?php
	class Catalog extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('catalog_model');
		}
		//lay danh sach danh muc san pham
		function index()
		{
			$list = $this->catalog_model->get_list();
			$this->data['list'] = $list;

			//
			//lấy nội dung của biến message
			$message = $this->session->flashdata('message');
			$this->data['message'] = $message;

			//load view
			$this->data['temp'] = 'admin/catalog/index';
			$this->load->view('admin/main',$this->data);
		}
		//them mới dữ liệu
		function add()
		{
			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','Tên','required');

			//nếu nhập chính xác
				if ($this->form_validation->run())
				{
					//them giu liệu
					$name = $this->input->post('name');
					$parent_id = $this->input->post('parent_id');
					$sort_order = $this->input->post('sort_order');
					$data = array(
						'name' => $name,
						'parent_id' =>$parent_id,
						'sort_order' =>intval($sort_order)
					);
					//them moi vao csdl
					if($this->catalog_model->create($data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Thêm Thành Công');
					}else{
						$this->session->set_flashdata('message','Không Thành Công');
					}
					// chuyển tới trang danh sách quản trị viên
					redirect(admin_url('catalog'));
				}
			}

			//lay danh sach danh muc cha
			$input = array();
			$input['where'] = array('parent_id' => 0);
			$list = $this->catalog_model->get_list($input);
			$this->data['list'] = $list;

			$this->data['temp']='admin/catalog/add';
			$this->load->view('admin/main',$this->data);
		}
		//cap nhat du lieu
		function edit()
		{
			$id = $this->uri->rsegment(3);
			$info = $this->catalog_model->get_info($id);
			if (!$info)
			{
				//tạo nội dung thông báo
				$this->session->set_flashdata('message','Không tồn tại thư mục');
				redirect(admin_url('catalog'));
			}
			$this->data['info'] = $info;
			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','Tên','required');

			//nếu nhập chính xác
				if ($this->form_validation->run())
				{
					//them giu liệu
					$name = $this->input->post('name');
					$parent_id = $this->input->post('parent_id');
					$sort_order = $this->input->post('sort_order');
					$data = array(
						'name' => $name,
						'parent_id' =>$parent_id,
						'sort_order' =>intval($sort_order)
					);
					//them moi vao csdl
					if($this->catalog_model->update($id,$data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Sửa Thành Công');
					}else{
						$this->session->set_flashdata('message','Không Thành Công');
					}
					// chuyển tới trang danh sách quản trị viên
					redirect(admin_url('catalog'));
				}
			}

			//lay danh sach danh muc cha
			$input = array();
			$input['where'] = array('parent_id' => 0);
			$list = $this->catalog_model->get_list($input);
			$this->data['list'] = $list;

			$this->data['temp']='admin/catalog/edit';
			$this->load->view('admin/main',$this->data);
		}

		//Hàm xóa dữ liệu
		function delete()
		{
			$id = $this->uri->rsegment('3');
			$id = intval($id);
			//lấy thông tin quản trị viên
			$info = $this->catalog_model->get_info($id);
			if (!$info)
			{
				$this->session->set_flashdata('message','Không tồn tại quản trị viên');
				redirect(admin_url('catalog'));
			}
			// xóa 
			$this->catalog_model->delete($id);
			//tao noi dung thong bao
			$this->session->set_flashdata('message','Xóa dữ liệu thành công');
			redirect(admin_url('catalog'));
		}
		//đăng xuất
		

	}
?>