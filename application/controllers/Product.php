<?php
	Class Product extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			//load model
			$this->load->model('product_model');
		}

		//hien thi danh sach san pham
		function catalog()
		{
			$this->load->model('catalog_model');
			//lay id the loai
			$id = $this->uri->rsegment(3);
			$id = intval($id);
			//lay thong tin the loai
			$catalog = $this->catalog_model->get_info($id);
			if (!$catalog)
			{
				redirect();
			}

			$this->data['catalog'] = $catalog;
			$input = array();

			//kiem tra xem day la danh muc con hay cha
			if ($catalog->parent_id == 0)
			{
				$input_catalog = array();
				$input_catalog['select'] = 'id';
				$input_catalog['where'] = array('parent_id' => $id );
				$catalog_sub = $this->catalog_model->get_list($input_catalog);
				if (!empty($catalog_sub)) // neu danh muc hien tai co danh muc con
				{
				$catalog_sub_id = array();
					foreach ($catalog_sub as $sub)
					{
						$catalog_sub_id[] = $sub->id;
					}
					//lay tat ca san pham thuoc cac danh muc con
					$this->db->where_in('catalog_id', $catalog_sub_id);				
				}else{
					  $input_catalog['where'] = array('catalog_id' => $id);
				}
			}else{
				$input_catalog['where'] = array('catalog_id' => $id);
			}


			//lay danh sach thuộc danh mục
			//lay tong so luong tat ca san pham
			$total_rows = $this->product_model->get_total($input);
			$this->data['total_rows'] = $total_rows;

			$config = array();
			$config['total_rows'] = $total_rows;// tong tat ca cac san pham
			$config['base_url'] = base_url('product/catalog/'.$id);//link hien thi danh sach san pham
			$config['per_page'] = 6;//so luong san pham hien thi
			$config['uri_segment'] = 4;// phan doan hien thi so trang url
			$config['next_link'] = 'Trang Kế';
			$config['prev_link'] = 'Trang Trước';
			//khởi tạo cấu hình pagination
			$this->pagination->initialize($config); 

			$segment = $this->uri->segment(4);
			$segment = intval($segment);

			$input['limit'] = array($config['per_page'],$segment);


			//lay danh sach san pham
			$list = $this->product_model->get_list($input);
			$this->data['list'] = $list;

			//hien thi ra view
			$this->data['temp'] = 'site/product/catalog';
			$this->load->view('site/layout',$this->data);
		}
		//xem chi tiet san pham
		function view()
		{
			//lay id san pham muon xem
			$id = $this->uri->rsegment(3);
			$product = $this->product_model->get_info($id);
			if (!$product) redirect();
			$this->data['product'] = $product;

			//lay danh sach san pham keo theo
			$image_list = json_decode($product->image_list);
			$this->data['image_list'] = $image_list;

			//cap nhat lai luot xem san pham
			$data = array();
			$data['view'] = $product->view + 1;
			$this->product_model->update($product->id,$data);

			//lay thong tin danh muc san pham

			$catalog = $this->catalog_model->get_info($product->catalog_id);
			$this->data['catalog'] = $catalog;

			//hien thi view
			$this->data['temp'] = 'site/product/view';
			$this->load->view('site/layout',$this->data);
		}
		// tim kiem theo ten spham
		function search()
		{
			$key = $this->input->get('key-search');
			$this->data['key'] = trim($key);


			$input = array();
			$input['like'] = array('name' , $key);
			$list = $this->product_model->get_list($input);
			$this->data['list'] = $list;


			//load view
			$this->data['temp'] = 'site/product/search';
			$this->load->view('site/layout',$this->data);
		}

		//tim kiem theo gia
		function search_price()
		{
			$price_from = intval($this->input->get('price_from'));
			$price_to   = intval($this->input->get('price_to'));
			$this->data['price_from'] = $price_from;
			$this->data['price_to']  = $price_to;
			//loc theo gia
			$input = array();
			$input['where'] = array('price >= ' => $price_from , 'price <=' => $price_to);
			$list = $this->product_model->get_list($input);
			//load view

			$this->data['list'] = $list;
			$this->data['temp'] = 'site/product/search_price';
			$this->load->view('site/layout',$this->data);
		}
	}
?>