<?php 
	Class Home extends MY_Controller
	{
		function index()
		{
			//load slide
			$this->load->model('slide_model');
			$slide_list = $this->slide_model->get_list();

			//load san pham
			$this->load->model('product_model');
			$input = array();
			$input['limit'] = array(3,0);
			$product_news = $this->product_model->get_list($input);
			//san pham xem nhieu
			$input['order'] = array('view','DESC');
			$product_view = $this->product_model->get_list($input);
			//san pham mua nhieu
			$input['order'] = array('buyed','DESC');
			$product_buyed = $this->product_model->get_list($input);

			$message = $this->session->flashdata('message');
			
			$this->data['message'] = $message;
			$this->data['product_view']  =$product_view;
			$this->data['product_buyed'] = $product_buyed;
			$this->data['slide_list'] 	 = $slide_list;
			$this->data['product_news']  = $product_news;
			$this->data['temp'] 	     = 'site/home/index';
			$this->load->view('site/layout',$this->data);
		}
	}
?>