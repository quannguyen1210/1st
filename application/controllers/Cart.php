<?php
	Class Cart extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();

		}

		//them san pham vao gio hang

		function add()
		{
			//lay san pham muon them gio hang
			$this->load->model('product_model');
			$id = $this->uri->rsegment(3);
			$product = $this->product_model->get_info($id);
			if(!$product)
			{
				redirect();
			}

			//tong so san pham
			$qty = 1;
			$price = $product->price;
			if($product->discount > 0)
			{
				$price = $product->price - $product->discount;
			}

			$data = array();
			$data['id'] 	     = $product->id;
			$data['qty'] 		 = $qty;
			$data['name'] 		 = url_title($product->name);
			$data['image_link']  = $product->image_link;
			$data['price']       = $price;

			$this->cart->insert($data);

			//chuyen sang trang them gio hang
			redirect(base_url('cart'));

		}

		//hien thi danh sach gio hang
		function index()
		{
			//thong tin gio hang
			$carts = $this->cart->contents();
			//tong so san pham co trong gio hang
			$total_items = $this->cart->total_items();

			$this->data['carts'] = $carts;
			$this->data['total_items'] = $total_items;

			$this->data['temp'] = 'site/cart/index';
			$this->load->view('site/layout',$this->data);
		}

		//cap nhat gio hang
		function update()
		{
			//tong so san pham co trong gio hang
			$carts = $this->cart->contents();

			foreach ($carts as $key => $row)
			{
				$total_qty = $this->input->post('qty_'.$row['id']);
				$data = array();
				$data['rowid'] = $key;
				$data['qty'] = $total_qty;
				$this->cart->update($data);
			}

			redirect(base_url('cart'));
		}

		//xoa gio hang
		function delete()
		{
			$id = $this->uri->rsegment(3);
			$id = intval($id);
			// truong hop xoa 1 san pham
			if($id > 0)
			{
			//tong so san pham co trong gio hang
			$carts = $this->cart->contents();

			foreach ($carts as $key => $row)
			{
				if($row['id'] == $id)
				{
					$data = array();
					$data['rowid'] = $key;
					$data['qty'] = 0;
					$this->cart->update($data);
				}
			}
			}else{
				// xoa toàn bộ giỏ hàng
				$this->cart->destroy();
			}
			redirect(base_url('cart'));
		}
	}
?>