<?php
	Class User extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('user_model');
		}

		// kiểm tra username
		function check_email()
		{
			$email = $this->input->post('email');
			$where = array('email' => $email);
			//kiểm tra email tồn tại
			if ($this->user_model->check_exists($where))
			{
				// Thông báo lỗi
				$this->form_validation->set_message(__FUNCTION__,'Email Tồn Tại');
				return false;
			}
			return true;
		}

		//dang ki
		function register()
		{

			//nếu có dữ liệu post lên
			if($this->input->post())
			{
				$this->form_validation->set_rules('email','Email đăng nhập','required|callback_check_email');
				$this->form_validation->set_rules('name','Tên','required|min_length[3]');
				$this->form_validation->set_rules('address','Địa Chỉ','required');
				$this->form_validation->set_rules('password','Password','required|min_length[6]');
				$this->form_validation->set_rules('re_password','Password','required|min_length[6]|matches[password]');
				$this->form_validation->set_rules('phone','Số Điện Thoại');
			//nếu nhập chính xác
				if ($this->form_validation->run())
				{
					//them giu liệu
					$password = $this->input->post('password');
					$data = array(
						'email' 	=> $this->input->post('email'),
						'address'	=> $this->input->post('address'),
						'phone'		=> $this->input->post('phone'),
						'name' 		=> $this->input->post('name'),
						'password'  =>md5($password)
					);
					if($this->user_model->create($data))
					{
						//tạo ra nội dung thông báo
						$this->session->set_flashdata('message','Đăng Kí Thành Công');
					}else{
						$this->session->set_flashdata('message','Đăng Kí Không Thành Công');
					}
					// chuyển tới trang danh sách quản trị viên
					redirect(base_url('home'));
				}
			}

			//load view
			$this->data['temp'] = 'site/user/register';
			$this->load->view('site/layout',$this->data);

		}

		function login()
		{
			if($this->input->post())
			{
				$this->form_validation->set_rules('email','Email đăng nhập','required|callback_check_email');
				$this->form_validation->set_rules('password','Password','required|min_length[6]');
				$this->form_validation->set_rules('login','login','callback_check_login');
				if($this->form_validation->run())
				{
					$this->session->set_userdata('user_login',true);

					redirect(base_url('home'));
				}
			}
			//view
			$this->data['temp'] = "site/user/login";
			$this->load->view('site/layout',$this->data);
		}

		function check_login()
		{
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$password= md5($password);

			$this->load->model('user_model');
			$where = array('email' => $email,
							'password' => $password);
			if($this->user_model->check_exists($where))
			{	
				return true;
			}
			$this->form_validation->set_message(__FUNCTION__,'Không Đăng Nhập Thành Công');
			return false;

		}
	}